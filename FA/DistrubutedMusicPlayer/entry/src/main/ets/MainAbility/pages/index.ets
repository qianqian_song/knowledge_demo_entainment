/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {PlayerManager, Music} from "../../Common/PlayerManager"
import {RemoteDeviceManager, RemoteDevice, RemoteDeviceStatus, getBundleName} from "../../Common/RemoteDeviceManager"
import CommonLog from "../../Common/CommonLog"
import {MusicListDialog} from "../../Common/components/MusicListDialog"
import {DeviceListDialog} from "../../Common/components/DeviceListDialog"
import prompt from '@system.prompt';
import featureAbility from '@ohos.ability.featureAbility';

@Entry
@Component
struct Index {
  private playerManager: PlayerManager = new PlayerManager()
  @State deviceList: RemoteDevice[]  = [
    { deviceId: 'LocalHost', deviceName: 'LocalHost', deviceType: 1, status: 1 }]
  private remoteDeviceManager: RemoteDeviceManager = new RemoteDeviceManager(this.deviceList)
  @State currentMusic: Music = this.playerManager.getCurrentMusic()
  @State isPlaying: boolean = false
  @State currentProgress: number = 0
  @State currentTimeText: string = '00:00'
  @State totalTimeText: string = '00:00'
  @State turntableAngle:number = 0
  @State pointerAngle:number = 0
  private turntableTimer:number
  private isSwitching: boolean= false
  private bundleName: string
  private packageName: string

// 音乐列表自定义弹窗
  musicListDialogController: CustomDialogController = new CustomDialogController({
    builder: MusicListDialog({
      musicList: this.playerManager.getMusicList(),
      currentMusic: this.playerManager.getCurrentMusic(),
      cancel: () => {
        CommonLog.info('onCancelMusicList')
      },
      confirm: (index: number) => {
        CommonLog.info('onChangeMusicList:' + index)
        this.playerManager.playSpecifyMusic(-1, index)
        this.isPlaying = true
      }
    }),
    cancel: this.existMusicListDialog,
    alignment: DialogAlignment.Bottom,
    autoCancel: true
  })

//设备列表自定义弹窗
  deviceListDialogController: CustomDialogController = new CustomDialogController({
    builder: DeviceListDialog({
      deviceList: this.deviceList,
      cancel: () => {
        CommonLog.info('onCancelDeviceListDialog')
      },
      confirm: (deviceName: string) => {
        CommonLog.info('Select from deviceListDialog, deviceName = ' + deviceName)

        if (deviceName == 'LocalHost') {
          CommonLog.info('Can not startAbility for LocalHost')
          return
        }

        let remoteDevices = this.deviceList.filter(device => device.deviceName == deviceName)

        if (remoteDevices.length == 0) {
          CommonLog.info('Can not find device for deviceName = ' + deviceName)
          return
        }
        let remoteDevice = remoteDevices[0]

        if (remoteDevice.status == RemoteDeviceStatus.UNTRUSTED) {
          // 认证设备
          this.remoteDeviceManager.authDevice(remoteDevice).then((data) => {
            //远程拉起
            this.startAbilityContinuation(remoteDevice)
          }).catch((err) => {
            prompt.showToast({
              message: 'Fail to authDevice. reason =' + err
            })
          })
        }
        else {
          //远程拉起
          this.startAbilityContinuation(remoteDevice)
        }
      }
    }),
    cancel: this.existDeviceListDialog,
    alignment: DialogAlignment.Bottom,
    offset:{dy:-40,dx:0},
    autoCancel: true
  })

  @Builder playPanel() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceBetween }) {
      Image($r("app.media.icon_music_changemode")).width(24).height(24).onClick(() => {
        this.onChangePlayMode()
      })
      Image($r("app.media.icon_music_left")).width(32).height(32).onClick(() => {
        this.onPreviousMusic()
      })
      Image(this.isPlaying ? $r("app.media.icon_music_play") : $r("app.media.icon_music_stop"))
        .width(80)
        .height(82)
        .onClick(() => {
          this.onPlayOrPauseMusic()
        })
      Image($r("app.media.icon_music_right")).width(32).height(32).onClick(() => {
        this.onNextMusic()
      })
      Image($r("app.media.icon_music_list")).width(24).height(24).onClick(() => {
        this.onShowMusicList()
      })
    }.width('100%').height(82)
  }

  @Builder operationPanel() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceBetween }) {
      Image($r("app.media.icon_music_like")).width(24).height(24)
      Image($r("app.media.icon_music_download")).width(24).height(24)
      Image($r("app.media.icon_music_comment")).width(24).height(24)
      Image($r("app.media.icon_music_more")).width(24).height(24)
    }.width('100%').height(49).padding({ bottom: 25 })
  }

  @Builder sliderPanel() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Text(this.currentTimeText).fontSize(12).fontColor("ff000000").width(40)
      Slider({
        value: this.currentProgress,
        min: 0,
        max: 100,
        step: 1,
        style: SliderStyle.OutSet
      })
        .blockColor(Color.White)
        .trackColor(Color.Gray)
        .selectedColor(Color.Blue)
        .showSteps(true)
        .flexGrow(1)
        .margin({ left: 5, right: 5 })
        .onChange((value: number, mode: SliderChangeMode) => {
          if (mode == 2) {
            CommonLog.info('currentProgress: ' + this.currentProgress)
            this.onChangeMusicProgress(value, mode)
          }
        })

      Text(this.totalTimeText).fontSize(12).fontColor("ff000000").width(40)

    }.width('100%').height(18)
  }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceBetween }) {
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center}) {
        Row(){
          Image($r("app.media.img_pointer")).objectFit(ImageFit.Contain).aspectRatio(1.32).width('40%').alignSelf(ItemAlign.Start)
            .rotate({
              x: 0,
              y: 0,
              z: 1,
              centerX: 0,
              centerY: 0,
              angle: this.pointerAngle
            })
        }.position({x:'50%',y:0}).zIndex(999)
        Image($r("app.media.icon_liuzhuan")).aspectRatio(1).width(80).alignSelf(ItemAlign.End).onClick(() => {
          this.onDistributeDevice()
        })
        Image($r("app.media.img_turntable")).width('80%').aspectRatio(1).objectFit(ImageFit.Contain)
          .rotate({
            x: 0,
            y: 0,
            z: 1,
            centerX: '50%',
            centerY: '50%',
            angle: this.turntableAngle
          }).margin({ top: 48 })

        Text(this.currentMusic.name).fontSize(20).fontColor("#e6000000").margin({ top: 10 })
        Text("未知音乐家").fontSize(14).fontColor("#99000000").margin({ top: 10 })
      }.flexGrow(1).width('100%')

      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.End }) {
        this.operationPanel()
        this.sliderPanel()
        this.playPanel()
      }.height(200)
    }
    .linearGradient({
      angle: 0,
      direction: GradientDirection.Bottom,
      colors: this.currentMusic.backgroundColor
    }).padding({ top: 48, bottom: 24, left: 24, right: 24 })
    .width('100%')
    .height('100%')
  }

  onPlayOrPauseMusic() {
    if (this.isSwitching) {
      CommonLog.info('onPlayOrPauseMusic ignored, isSwitching')
    }
    this.isSwitching = true

    if (this.isPlaying) {
      CommonLog.info("Start to pause music")
      this.playerManager.pause()
      this.pauseAnimation()
    }
    else {
      CommonLog.info("Start to play music")
      this.playerManager.play(-1)
      this.playAnimation()
    }
    this.isPlaying = !this.isPlaying
    this.isSwitching = false
  }

  playAnimation(){
    if (this.turntableTimer) {
      return
    }
    let fun = () => {
      animateTo({ duration: 1500, curve: Curve.Linear, playMode: PlayMode.Normal },
        () => {
          this.turntableAngle = this.turntableAngle + 90;
        })
    }
    fun()
    this.turntableTimer = setInterval(fun, 1600)

    animateTo({ duration: 1500, curve: Curve.Linear, playMode: PlayMode.Normal },
      () => {
        this.pointerAngle = 39;
      })
  }

  pauseAnimation(){
    if (this.turntableTimer) {
      clearInterval(this.turntableTimer)
      this.turntableTimer = undefined
    }

    animateTo({ duration: 1500, curve: Curve.Linear, playMode: PlayMode.Normal },
      () => {
        this.pointerAngle = 0;
      })
  }

  onNextMusic() {
    CommonLog.info("Play next music")
    if (this.isSwitching) {
      CommonLog.info('onNextMusic ignored, isSwitching');
      return;
    }

    this.isSwitching = true
    this.playerManager.playNextMusic()
    this.playAnimation()
    this.isPlaying = true
    this.isSwitching = false
  }

  onPreviousMusic() {
    CommonLog.info("Play previous music")
    if (this.isSwitching) {
      CommonLog.info('onPreviousMusic ignored, isSwitching');
      return;
    }

    this.isSwitching = true
    this.playerManager.playPreviousMusic()
    this.playAnimation()
    this.isPlaying = true
    this.isSwitching = false
  }

  onShowMusicList() {
    CommonLog.info("Show music list")
    this.musicListDialogController.open()
  }

  existMusicListDialog() {
    CommonLog.info('existMusicListDialog in the blank area')
  }

  existDeviceListDialog() {
    CommonLog.info('existDeviceListDialog')
  }

  onChangePlayMode() {
    this.playerManager.changePlayMode()
    CommonLog.info("Change play mode")
  }

  onDistributeDevice() {
    CommonLog.info("onDistributeDevice")
    // 显示设备列表窗口
    this.remoteDeviceManager.refreshRemoteDeviceList()
    this.deviceListDialogController.open()
  }

  onChangeMusicProgress(value: number, mode: SliderChangeMode) {
    if (this.isSwitching) {
      CommonLog.info('onChangeMusicProgress ignored, isSwitching');
      return;
    }
    this.isSwitching = true
    CommonLog.info('value:' + value + 'mode:' + mode.toString())
    this.currentProgress = value
    if (this.totalTimeText != '00:00') {
      let currentMS = this.currentProgress / 100 * this.playerManager.getTotalTimeMs()
      this.currentTimeText = this.getShownTimer(currentMS)
      CommonLog.info('onChangeMusicProgress:' + currentMS)
      this.playerManager.seek(currentMS)
    } else {
      CommonLog.info('onChangeMusicProgress:bbb')
      this.currentProgress = 0
    }
    this.isSwitching = false
  }

  async aboutToAppear() {
    this.grantPermission()
    CommonLog.info("aboutToAppear")
    this.playerManager.setOnPlayingProgressListener((currentTimeMs) => {
      this.currentTimeText = this.getShownTimer(currentTimeMs)
      this.currentProgress = Math.floor(currentTimeMs / this.playerManager.getTotalTimeMs() * 100)
      if (this.totalTimeText == '00:00') {
        this.totalTimeText = this.getShownTimer(this.playerManager.getTotalTimeMs())
      }
    })

    this.playerManager.setOnStatusChangeListener(() => {
      CommonLog.info('setOnStatusChangeListener ' + this.playerManager.getCurrentMusic()
      .url)
      //1.刷新歌曲名和背景
      this.currentMusic = this.playerManager.getCurrentMusic()

      //2.刷新播放时间
      this.totalTimeText = this.getShownTimer(this.playerManager.getTotalTimeMs())
      this.currentTimeText = '00:00'

      //3.刷新播放状态
      this.isPlaying = true
    })

    // 分布式拉起还原
    this.restoreFromWant()

    //获取包名
    getBundleName().then((data) => {
      this.bundleName = data
    })

    // 启动设备扫描
    this.remoteDeviceManager.startDeviceDiscovery()
  }

  grantPermission() {
        console.info('MusicPlayer[IndexPage] grantPermission')
        let context = featureAbility.getContext()
        context.requestPermissionsFromUser(['ohos.permission.DISTRIBUTED_DATASYNC'], 666, function (result:any) {
            console.info(`MusicPlayer[IndexPage] grantPermission,requestPermissionsFromUser,result.requestCode=${result.requestCode}`)
        })
  }

  aboutToDisappear() {
    CommonLog.info('aboutToDisappear');
    this.remoteDeviceManager.stopDeviceDiscovery()
  }


/**
   * 计算显示时间
   */
  getShownTimer(ms) {
    let seconds = Math.floor(ms / 1000);
    let sec = seconds % 60;
    let min = (seconds - sec) / 60;

    let secString = sec.toString()
    let minString = min.toString()
    if (sec < 10) {
      secString = '0' + sec;
    }
    if (min < 10) {
      minString = '0' + min;
    }
    return minString + ':' + secString;
  }

/**
   * 分布式拉起，并结束当前应用
   */
  startAbilityContinuation(remoteDevice:RemoteDevice) {
    let params = {
      index: this.playerManager.getCurrentMusicIndex(),
      seekTo: this.playerManager.getCurrentTimeMs(),
      isPlaying: this.isPlaying
    }
    CommonLog.info('startAbilityContinuation deviceId=' + remoteDevice.deviceId
    + ' deviceName=' + remoteDevice.deviceName)
    CommonLog.info('startAbilityContinuation params=' + JSON.stringify(params))
    CommonLog.info('startAbilityContinuation ' + this.bundleName)

    let wantValue = {
      bundleName: this.bundleName,
      abilityName: 'com.madixin.music.MainAbility',
      deviceId: remoteDevice.deviceId,
      parameters: params
    }

    featureAbility.startAbility({
      want: wantValue
    }).then((data) => {
      CommonLog.info('startAbilityContinuation finished, ' + JSON.stringify(data))

      //自我关闭
      featureAbility.terminateSelf((error) => {
        CommonLog.info('startAbilityContinuation terminateSelf finished, error=' + JSON.stringify(error))
      })
    }).catch((error) => {
      CommonLog.info('startAbilityContinuation error ' + JSON.stringify(error))
      prompt.showToast({
        message: 'startAbilityContinuation error ' + JSON.stringify(error)
      })
    })
  }

/**
   * 分布式拉起还原当前播放歌曲
   */
  restoreFromWant() {
    featureAbility.getWant((error, want) => {
      CommonLog.info('restoreFromWant featureAbility.getWant=' + JSON.stringify(want))
      let status = want.parameters
      if (status != null && status.index != null) {
        this.playerManager.playSpecifyMusic(status.seekTo, status.index)
        this.isPlaying = true
        this.playAnimation()
      }
    })
  }
}
