/*
 * Copyright (c) 2021 Cocollria
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var Search_Map = null;
var Attack_Map = null;

var Search_ROW = 0;
var Search_COL = 0;

export function find_Search_Map_1(map, row, col, x, y, move_point){
    //首先生成一个搜寻图的二维数组(先在只有普通的障碍物(包括敌人)的情况下,进行寻路)
    Search_ROW = row;
    Search_COL = col;
    Search_Map = new Array();
    for(var i = 0; i < Search_ROW; i++){
        Search_Map[i] = new Array();
        for(var j = 0; j < Search_COL; j++){
            if((map[i][j] <= 100 && map[i][j] > 0) || (map[i][j] > 200 && map[i][j] <= 300)){
                Search_Map[i][j] = -1;
            }
            else{
                Search_Map[i][j] = map[i][j];
            }
        }
    }
    Search_Map[y][x] = -1;

    var cost_point = 1;         //花费的移动点
    //开始寻路
    if(cost_point <= move_point) {
        if ((y - 1 >= 0) && (Search_Map[y - 1][x] == 0 || (Search_Map[y - 1][x] > 0 && Search_Map[y - 1][x] > cost_point))) {
            Search_Map[y - 1][x] = cost_point;
            //判断是否还有移动点继续移动，有则进入continue_to_find_1函数递归寻路
            if(cost_point + 1 <= move_point) {
                continue_to_find_1(x, y - 1, cost_point + 1, move_point);
            }
        }

        if ((x - 1 >= 0) && (Search_Map[y][x - 1] == 0 || (Search_Map[y][x - 1] > 0 && Search_Map[y][x - 1] > cost_point))) {
            Search_Map[y][x - 1] = cost_point;
            //同上注释
            if(cost_point + 1 <= move_point) {
                continue_to_find_1(x - 1, y, cost_point + 1, move_point);
            }
        }
        if ((x + 1 < Search_COL) && (Search_Map[y][x + 1] == 0 || (Search_Map[y][x + 1] > 0 && Search_Map[y][x + 1] > cost_point))) {
            Search_Map[y][x + 1] = cost_point;
            //同上注释
            if(cost_point + 1 <= move_point) {
                continue_to_find_1(x + 1, y, cost_point + 1, move_point);
            }
        }
        if ((y + 1 < Search_ROW) && (Search_Map[y + 1][x] == 0 || (Search_Map[y + 1][x] > 0 && Search_Map[y + 1][x] > cost_point))) {
            Search_Map[y + 1][x] = cost_point;
            //同上注释
            if(cost_point + 1 <= move_point) {
                continue_to_find_1(x, y + 1, cost_point + 1, move_point);
            }
        }
    }

    //开始寻找周围可攻击的敌人
    find_Attack_Map_1(map, x, y);
}

function continue_to_find_1(x, y, cost, move_point) {
    //如果该点没有走过同时没有超过边界或者走过所花费的点大于现在花费的点数，那表示可以继续走
    if((y - 1 >= 0) && (Search_Map[y - 1][x] == 0 || (Search_Map[y - 1][x] > 0 && Search_Map[y - 1][x] > cost))) {
        Search_Map[y - 1][x] = cost;
        //判断是否还有移动点继续移动，有的话继续递归寻路
        if(cost + 1 <= move_point) {
            continue_to_find_1(x, y - 1, cost + 1, move_point);
        }
    }
    if((x - 1 >= 0) && (Search_Map[y][x - 1] == 0 || (Search_Map[y][x - 1] > 0 && Search_Map[y][x - 1] > cost))) {
        Search_Map[y][x - 1] = cost;
        //同上注释
        if(cost + 1 <= move_point) {
            continue_to_find_1(x - 1, y, cost + 1, move_point);
        }
    }
    if((x + 1 < Search_COL) && (Search_Map[y][x + 1] == 0 || (Search_Map[y][x + 1] > 0 && Search_Map[y][x + 1] > cost))) {
        Search_Map[y][x + 1] = cost;
        //同上注释
        if(cost + 1 <= move_point) {
            continue_to_find_1(x + 1, y, cost + 1, move_point);
        }
    }
    if((y + 1 < Search_ROW) && (Search_Map[y + 1][x] == 0 || (Search_Map[y + 1][x] > 0 && Search_Map[y + 1][x] > cost))) {
        Search_Map[y + 1][x] = cost;
        //同上注释
        if(cost + 1 <= move_point) {
            continue_to_find_1(x, y + 1, cost + 1, move_point);
        }
    }
}

//寻找周围可攻击的敌人
export function find_Attack_Map_1(map, x, y){
    var ret = false;
    Attack_Map = new Array();
    //先在只有普通的障碍物(包括敌人)的情况下,进行寻路
    for(var i = 0; i < Search_ROW; i++){
        Attack_Map[i] = new Array();
        for(var j = 0; j < Search_COL; j++){
            if(map[i][j] > 0){
                Attack_Map[i][j] = -1;
            }
            else{
                Attack_Map[i][j] = 0;
            }
        }
    }
    if((y - 1 >= 0) && (map[y - 1][x] > 200 && map[y - 1][x] <= 300)) {
        Attack_Map[y - 1][x] = 1;
        ret = true;
    }
    if((x - 1 >= 0) && (map[y][x - 1] > 200 && map[y][x - 1] <= 300)) {
        Attack_Map[y][x - 1] = 1;
        ret = true;
    }
    if((x + 1 < Search_COL) && (map[y][x + 1] > 200 && map[y][x + 1] <= 300)) {
        Attack_Map[y][x + 1] = 1;
        ret = true;
    }
    if((y + 1 < Search_ROW) && (map[y + 1][x] > 200 && map[y + 1][x] <= 300)) {
        Attack_Map[y + 1][x] = 1;
        ret = true;
    }
    return ret;
}





//敌方寻路的算法
export function find_Search_Map_2(map, row, col, x, y, move_point){
    //首先生成一个搜寻图的二维数组(先在只有普通的障碍物(包括敌人)的情况下,进行寻路)
    Search_ROW = row;
    Search_COL = col;
    Search_Map = new Array();
    for(var i = 0; i < Search_ROW; i++){
        Search_Map[i] = new Array();
        for(var j = 0; j < Search_COL; j++){
            if(map[i][j] <= 200 && map[i][j] > 0){
                Search_Map[i][j] = -1;
            }
            else{
                Search_Map[i][j] = map[i][j];
            }
        }
    }
    Search_Map[y][x] = -1;

    var cost_point = 1;         //花费的移动点
    //开始寻路
    if(cost_point <= move_point) {
        if ((y - 1 >= 0) && (Search_Map[y - 1][x] == 0 || (Search_Map[y - 1][x] > 0 && Search_Map[y - 1][x] > cost_point))) {
            Search_Map[y - 1][x] = cost_point;
            //判断是否还有移动点继续移动，有则进入continue_to_find_2函数递归寻路
            if(cost_point + 1 <= move_point) {
                continue_to_find_2(x, y - 1, cost_point + 1, move_point);
            }
        }

        if ((x - 1 >= 0) && (Search_Map[y][x - 1] == 0 || (Search_Map[y][x - 1] > 0 && Search_Map[y][x - 1] > cost_point))) {
            Search_Map[y][x - 1] = cost_point;
            //同上注释
            if(cost_point + 1 <= move_point) {
                continue_to_find_2(x - 1, y, cost_point + 1, move_point);
            }
        }
        if ((x + 1 < Search_COL) && (Search_Map[y][x + 1] == 0 || (Search_Map[y][x + 1] > 0 && Search_Map[y][x + 1] > cost_point))) {
            Search_Map[y][x + 1] = cost_point;
            //同上注释
            if(cost_point + 1 <= move_point) {
                continue_to_find_2(x + 1, y, cost_point + 1, move_point);
            }
        }
        if ((y + 1 < Search_ROW) && (Search_Map[y + 1][x] == 0 || (Search_Map[y + 1][x] > 0 && Search_Map[y + 1][x] > cost_point))) {
            Search_Map[y + 1][x] = cost_point;
            //同上注释
            if(cost_point + 1 <= move_point) {
                continue_to_find_2(x, y + 1, cost_point + 1, move_point);
            }
        }
    }

    //开始寻找周围可攻击的己方棋子
    find_Attack_Map_2(map, x, y);
}

function continue_to_find_2(x, y, cost, move_point) {
    //如果该点没有走过同时没有超过边界或者走过所花费的点大于现在花费的点数，那表示可以继续走
    if((y - 1 >= 0) && (Search_Map[y - 1][x] == 0 || (Search_Map[y - 1][x] > 0 && Search_Map[y - 1][x] > cost))) {
        Search_Map[y - 1][x] = cost;
        //判断是否还有移动点继续移动，有的话继续递归寻路
        if(cost + 1 <= move_point) {
            continue_to_find_2(x, y - 1, cost + 1, move_point);
        }
    }
    if((x - 1 >= 0) && (Search_Map[y][x - 1] == 0 || (Search_Map[y][x - 1] > 0 && Search_Map[y][x - 1] > cost))) {
        Search_Map[y][x - 1] = cost;
        //同上注释
        if(cost + 1 <= move_point) {
            continue_to_find_2(x - 1, y, cost + 1, move_point);
        }
    }
    if((x + 1 < Search_COL) && (Search_Map[y][x + 1] == 0 || (Search_Map[y][x + 1] > 0 && Search_Map[y][x + 1] > cost))) {
        Search_Map[y][x + 1] = cost;
        //同上注释
        if(cost + 1 <= move_point) {
            continue_to_find_2(x + 1, y, cost + 1, move_point);
        }
    }
    if((y + 1 < Search_ROW) && (Search_Map[y + 1][x] == 0 || (Search_Map[y + 1][x] > 0 && Search_Map[y + 1][x] > cost))) {
        Search_Map[y + 1][x] = cost;
        //同上注释
        if(cost + 1 <= move_point) {
            continue_to_find_2(x, y + 1, cost + 1, move_point);
        }
    }
}

//寻找周围可攻击的己方棋子
export function find_Attack_Map_2(map, x, y){
    var ret = false;
    Attack_Map = new Array();
    //先在只有普通的障碍物(包括敌人)的情况下,进行寻路
    for(var i = 0; i < Search_ROW; i++){
        Attack_Map[i] = new Array();
        for(var j = 0; j < Search_COL; j++){
            if(map[i][j] > 0){
                Attack_Map[i][j] = -1;
            }
            else{
                Attack_Map[i][j] = 0;
            }
        }
    }
    if((y - 1 >= 0) && (map[y - 1][x] > 100 && map[y - 1][x] <= 200)) {
        Attack_Map[y - 1][x] = 1;
        ret = true;
    }
    if((x - 1 >= 0) && (map[y][x - 1] > 100 && map[y][x - 1] <= 200)) {
        Attack_Map[y][x - 1] = 1;
        ret = true;
    }
    if((x + 1 < Search_COL) && (map[y][x + 1] > 100 && map[y][x + 1] <= 200)) {
        Attack_Map[y][x + 1] = 1;
        ret = true;
    }
    if((y + 1 < Search_ROW) && (map[y + 1][x] > 100 && map[y + 1][x] <= 200)) {
        Attack_Map[y + 1][x] = 1;
        ret = true;
    }
    return ret;
}





//获取移动的路线
export function get_move_route(click_x, click_y, x, y){
    var route = new Array();
    var cost_point = Search_Map[click_y][click_x];
    var cx = click_x;
    var cy = click_y;
    //先反向寻路(从目的地到出发地)
    while(cost_point > 1){
        if(cy - 1 >= 0 && Search_Map[cy - 1][cx] == cost_point - 1){
            cy -= 1;
            //1代表向下走
            route.push(1);
        }
        else if(cx - 1 >= 0 && Search_Map[cy][cx - 1] == cost_point - 1){
            cx -= 1;
            //2代表向右走
            route.push(2);
        }
        else if(cx + 1 < Search_ROW && Search_Map[cy][cx + 1] == cost_point - 1){
            cx += 1;
            //3代表向左走
            route.push(3);
        }
        else if(cy + 1 < Search_COL && Search_Map[cy + 1][cx] == cost_point - 1){
            cy += 1;
            //4代表向上走
            route.push(4);
        }
        cost_point--;
    }
    //最后一步从出发地到目的地(正常走,因为出发点的cost被置为-1了，只能从出发地开始找)
    if(cy - y == 1){
        route.push(1);
    }
    else if(cx - x == 1){
        route.push(2);
    }
    else if(cx - x == -1){
        route.push(3);
    }
    else if(cy - y == -1){
        route.push(4);
    }

//    console.log("route = " + route);
    return route;
}

//获取寻路图
export function get_Search_Map(){
    return Search_Map;
}

//获取周围可攻击敌人的图
export function get_Attack_Map(){
    return Attack_Map;
}

//清理数据
export function clearMap(){
    Search_Map = null;
    Attack_Map = null;
}

//function printf_Search_Map(){
//    for(var i = 0; i < Search_ROW; i++){
//        console.log(
//            " " + Search_Map[i][0] +
//            " " + Search_Map[i][1] +
//            " " + Search_Map[i][2] +
//            " " + Search_Map[i][3] +
//            " " + Search_Map[i][4] +
//            " " + Search_Map[i][5] +
//            " " + Search_Map[i][6] +
//            " " + Search_Map[i][7] +
//            " " + Search_Map[i][8] +
//            " " + Search_Map[i][9] +
//            " " + Search_Map[i][10] +
//            " " + Search_Map[i][11] +
//            " " + Search_Map[i][12]);
//    }
//    console.log("==========================================");
//}

//function printf_Attack_Map(){
//    for(var i = 0; i < Search_ROW; i++){
//        console.log(
//            " " + Attack_Map[i][0] +
//            " " + Attack_Map[i][1] +
//            " " + Attack_Map[i][2] +
//            " " + Attack_Map[i][3] +
//            " " + Attack_Map[i][4] +
//            " " + Attack_Map[i][5] +
//            " " + Attack_Map[i][6] +
//            " " + Attack_Map[i][7] +
//            " " + Attack_Map[i][8] +
//            " " + Attack_Map[i][9] +
//            " " + Attack_Map[i][10] +
//            " " + Attack_Map[i][11] +
//            " " + Attack_Map[i][12]);
//    }
//    console.log("==========================================");
//}