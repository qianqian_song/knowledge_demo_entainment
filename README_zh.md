# knowledge_demo_entainment

## 简介
基于影音娱乐场景打造相关OpenHarmony音视频和游戏等样例，开发者不仅可以体验到最新最全的OpenHarmony音视频特性、应用开发框架，而且还可以从分布式流转等特性中感受万物互联带来的美妙。

## 样例汇总

#### 连接模组类应用

+ [Hi3861V100开发板轻量系统上手-两只老虎](docs/hi3861_v100_demo_music)
+ [Hi3861V100开发板轻量系统上手-视频播放](docs/hi3861_v100_demo_oledplayer)

#### 标准系统应用

- [OpenHarmony 分布式传炸弹小游戏 ](docs/BombGame/readme.md)
- [OpenHarmony 分布式音乐播放器 ](docs/DistrubutedMusicPlayer/README.md)
- [DataSharedDemo 意见分歧解决器 ](docs/DataSharedDemo)
- [Distrubuted24Game 益智24点小游戏 ](docs/Distrubuted24Game)
- [OpenHarmony 分布式音乐播放器 ](docs/DistrubutedMusicPlayer)
- [OpenHarmony 拼图小游戏 ](docs/Jigsaw)
- [OpenHarmony上跑起ArkUI小游戏 ](docs/SnakeGame)
- [OpenHarmony 分布式井字过三关小游戏 ](docs/TicTacToeGame)
- [WarChess ](docs/WarChess)

## 参考资料

+ [OpenHarmony官网](https://www.openharmony.cn/)
+ [OpenHarmony知识体系仓库](https://gitee.com/openharmony-sig/knowledge)
+ [OpenHarmony知识体系议题申报](https://docs.qq.com/sheet/DUUNpcWR6alZkUmFO)
+ [OpenHarmony知识体系例会纪要](https://gitee.com/openharmony-sig/sig-content/tree/master/knowlege/meetings)
+ [OpenHarmony开发样例共建说明](https://gitee.com/openharmony-sig/knowledge/blob/master/docs/co-construct_demos/README_zh.md)
+ [OpenHarmony知识体系-智能管家样例](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)
+ [OpenHarmony知识体系-智能出行场景](https://gitee.com/openharmony-sig/knowledge_demo_travel)
+ [OpenHarmony知识体系-购物消费场景](https://gitee.com/openharmony-sig/knowledge_demo_shopping)

