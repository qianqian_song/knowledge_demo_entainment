# Pegasus智能家居套件样例开发--PWM（pwm_beer）

![wifi_iot](https://gitee.com/hihopeorg/hispark-hm-pegasus/raw/master/docs/figures/2.png)

## 一、PWM API

| API名称                                                      | 说明              |
| ------------------------------------------------------------ | ----------------- |
| unsigned int IoTPwmInit(WifiIotPwmPort port);                   | PWM模块初始化     |
| unsigned int IoTPwmStart(WifiIotPwmPort port, unsigned short duty, unsigned short freq); | 开始输出PWM信号   |
| unsigned int IoTPwmStop(WifiIotPwmPort port);                   | 停止输出PWM信号   |
| unsigned int IoTPwmDeinit(WifiIotPwmPort port);                 | 解除PWM模块初始化 |
| unsigned int PwmSetClock(WifiIotPwmClkSource clkSource);     | 设置PWM模块时钟源 |

### 说明：

###### 本样例适用版本：OpenHarmony1.0.1~3.0

###### OpenHarmony源码下载：https://gitee.com/openharmony/docs/tree/master/zh-cn/release-notes

###### Demo源码下载：https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/dev/team_x/demo_music

## 二、交通灯板蜂鸣器与主控芯片（Pegasus）引脚的对应关系

- **蜂鸣器：**GPIO9/PWM0/输出PWM波控制蜂鸣器发出声音

  [详细说明](https://harmonyos.51cto.com/posts/1511)

## 三、如何编译

1. 将此目录下的 `beeper_music_demo.c` 和 `BUILD.gn` 复制到openharmony源码的`applications\sample\wifi-iot\app\iothardware`目录下，
2. 修改openharmony源码的`applications\sample\wifi-iot\app\BUILD.gn`文件，将其中的 `features` 改为：

```
    features = [
        "iothardware:beeper_music_demo",
    ]
```

   3.在openharmony源码顶层目录执行：`hb set`、`.`，选择需要编译的版本`wifiiot_hispark_pegasus`

   4.执行`hb build`启动版本构建

#### 报错解决

1. 编译过程中报错：undefined reference to`hi_pwm_init`等几个`hi_pwm_`开头的函数，
   - **原因：** 因为默认情况下，hi3861_sdk中，PWM的CONFIG选项没有打开
   - **解决：**修改`device/hisilicon/hispark_pegasus/sdk_liteos/build/config/usr_config.mk`文件中的`CONFIG_PWM_SUPPORT`行：
     - `# CONFIG_PWM_SUPPORT is not set`修改为`CONFIG_PWM_SUPPORT=y

## 四、运行结果

烧录文件后，按下reset按键，程序开始运行，蜂鸣器会播放《两只老虎》

